section .text

%define NEWLINE 0xA
%define HORIZONTAL_TAB 0x9
%define SPACE 0x20
%define NULL_TERMINATOR 0x00
%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
 .count:
	cmp byte[rdi+rax], NULL_TERMINATOR
	je .end
	inc rax
	jmp .count
 .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi

	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, 1
	syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
	mov rax, SYS_WRITE
	dec rsp
	mov [rsp], dil
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	inc rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, NEWLINE
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	xor rdx, rdx
	mov r10, rsp
	dec rsp
	mov byte[rsp], dl
 .loop:
    mov r11, 10
	div r11
	add rdx, '0'
	dec rsp
	mov byte[rsp], dl
	xor rdx, rdx
	test rax, rax
	jnz .loop
	mov rdi, rsp

	push r10
	call print_string
	pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
	cmp rdi, 0
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
 .print:
	jmp print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	xor r10, r10
	xor r11, r11
 .loop:
	mov r10b, [rdi]
	mov r11b, [rsi]
	cmp r10b, r11b
	jne .non_equal
	inc rdi
	inc rsi
	test r10b, r10b
	jnz .loop
	mov rax, 1
	jmp .end
 .non_equal:
	xor rax, rax
 .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
	xor rdi, rdi
	dec rsp
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	jz .end
	mov al, [rsp]
 .end:
	inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        push r12
        push r13
        push r14
        mov r12, rdi
        mov r13, rsi
.loop:
        cmp r14, r13
        jge .fail
        call read_char

        cmp al, SPACE
        je .check_space
        cmp al, HORIZONTAL_TAB
        je .check_space
        cmp al, NEWLINE
        je .check_space
.save:
        mov [r12 + r14], al
        cmp al, NULL_TERMINATOR
        je .success
        inc r14
        jmp .loop
.check_space:
        test r14, r14
        jz .loop

.success:
        mov rax, r12
        mov rdx, r14
        jmp .end
.fail:
        xor rax, rax
.end:
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
 .loop:
	cmp byte[rdi+rdx], '0'
	jl .end
	cmp byte[rdi+rdx], '9'
	jg .end
	push rdx
	mov rdx, 10
	mul rdx
	pop rdx
	xor r11, r11
	mov r11b, byte[rdi+rdx]
	add rax, r11
	sub rax, '0'
	inc rdx
	jmp .loop
 .end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], '-'
	je .neg
	jmp parse_uint
 .neg:
	inc rdi
	call parse_uint
	inc rdx
	neg rax

    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
 .loop:
	cmp rax, rdx
	jge .fail
	mov r10b, byte[rdi+rax]
	mov byte[rsi+rax], r10b
	cmp byte[rsi+rax], NULL_TERMINATOR
	je .end
	inc rax
	jmp .loop
 .fail:
	xor rax, rax
 .end:
    ret

